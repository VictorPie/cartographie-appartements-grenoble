import csv, os, sqlite3, pandas
from sqlite3 import Error

""" ---- Déclarations initiales ---- """
MonFichierBD = "DataBaseSQLite-Immobilier.db"
MonDossierDeDataSource = "Data/"
""" -- Parametres initiaux -- """
ListeAnnee = [2018] # defini le nombre d'année à récupérer
TableCible = "appart" # Une table de la BD SQLite
FichierDeTraitement = "Data2018-38185.csv" # Fichier intermédiaire
CodeVilleCible = "38185" # en l'occurrence Grenoble
CodeTypeCible = "2" # en l'occurrence Appartement
NatureMutationCible = "Vente"

""" ---- Traitement du Data Source ---- """
def csv_Verif(): #supprime le csv s'il existe
    if os.path.exists(FichierDeTraitement):
        os.remove(FichierDeTraitement)
        print("Un nouveau fichier source est en cours de traitement et remplacera le précédant")
        csv_Extraction()
    else:
        print("Un fichier source est en cours de traitement")
        csv_Extraction()

def csv_Extraction(): #crée un nouveau csv avec seulement Grenoble et les colonnes désirées
    for Annee, Liste in enumerate(ListeAnnee):
        with open(f"Data/immo_{ListeAnnee[Annee]}.csv", newline='') as csvfiler:
            lecture = csv.reader(csvfiler, delimiter=',')
            with open(FichierDeTraitement, 'a') as csvfilew:
                ecriture = csv.writer(csvfilew, delimiter=",")
                for row in lecture:
                    header = row[4], row[31], row[38], row[39]
                    ecriture.writerow(header)
                    break
                for row in lecture:
                    if  row[10] == CodeVilleCible and row[29] == CodeTypeCible and row[3] == NatureMutationCible:
                        cequonveut = row[4], row[31], row[38], row[39]
                        ecriture.writerow(cequonveut)
    print("Un fichier source de data a été traité avec succès")

""" ---- Creation de la BD d'éxécution ---- """

def MaCreationDeConnectionSQLite(UnFichierBD):
    MonConnecteur = None
    try:
        MonConnecteur = sqlite3.connect(MonFichierBD)
    except Error as UneErreurDeConnection:
        print(UneErreurDeConnection)
    return MonConnecteur

def MaCreationDeTable(MonConnecteur): # Pandas
    #MonConnecteur.execute("""CREATE TABLE idpk (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL);""")
    ObjetPandas = pandas.read_csv(FichierDeTraitement)
    ObjetPandas.to_sql(TableCible, MonConnecteur, if_exists='replace', index=True, index_label="idpk")
    # MonConnecteur.execute(f"""ALTER TABLE idpk
    #                         INSERT INTO idpk SELECT * FROM {TableCible};
    #                         DROP TABLE {TableCible};
    #                         RENAME TO {TableCible};
    #                         """)

    #ALTER TABLE {TableCible} ADD INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL;")
    #dtype={"Code type local":int,"Valeur foncière":float}
    print(f"Table SQL {TableCible} intégrée avec succès à la basse de données")

def AjoutDansTable(MonConnecteur): # Pandas
    wip()
    # ObjetPandas = pandas.read_csv(FichierDeTraitement)
    # ObjetPandas.to_sql(TableCible, MonConnecteur, if_exists='append', index=True, index_label="idpk")
    # print(f"La table SQL {TableCible} à été mise à jour dans la basse de données")

def main():
    Menu = """Que voulez-vous faire :
            1 - Traiter/Re-traiter un fichier source.
            2 - Créer/Re-créer la base de données.
            3 - Ajouter les données traitées à la base de données existante.
            4 - Changer les paramètres initiaux.
            Q - Sortir."""
    print(Menu)
    i = 0
    while i == 0:
        Choix = input(">> Votre choix ? : ")
        if Choix == "1":
            csv_Verif()
        elif Choix == "2":
            MonConnecteur = MaCreationDeConnectionSQLite(MonFichierBD)
            MaCreationDeTable(MonConnecteur)
        elif Choix == "3":
            MonConnecteur = MaCreationDeConnectionSQLite(MonFichierBD)
            AjoutDansTable(MonConnecteur)
        elif Choix == "4":
            wip()
        elif Choix == "Q":
            break
            i = 1
        else:
            print("Veuillez entrer un choix valide !")

def wip():
    print("Développement en cours")
    
if __name__=="__main__":  
    main()